import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InfoComponent } from './components/info/info.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { UsersettingsComponent } from './components/usersettings/usersettings.component';
import { GrupoinfoComponent } from './components/grupoinfo/grupoinfo.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MybillsComponent } from './components/mybills/mybills.component';
import { NotificacaoComponent } from './components/notificacao/notificacao.component';
import { HttpClientModule } from '@angular/common/http';
import { FororforComponent } from './components/fororfor/fororfor.component';
import { CreategroupComponent } from './creategroup/creategroup.component';






@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InfoComponent,
    RegisterComponent,
    LoginComponent,
    UsersettingsComponent,
    GrupoinfoComponent,
    MybillsComponent,
    NotificacaoComponent,
    FororforComponent,
    CreategroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
