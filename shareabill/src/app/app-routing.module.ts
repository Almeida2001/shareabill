import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FororforComponent } from './components/fororfor/fororfor.component';
import { GrupoinfoComponent } from './components/grupoinfo/grupoinfo.component';
import { InfoComponent } from './components/info/info.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';
import { UsersettingsComponent } from './components/usersettings/usersettings.component';
import { MybillsComponent } from './components/mybills/mybills.component';

const routes: Routes = [
  { path: '', component: InfoComponent},
  { path: 'info', component: InfoComponent},
  { path: 'registo', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'usersettings', component: UsersettingsComponent},
  { path: 'groupinfo', component: GrupoinfoComponent},
  { path: 'mybills', component: MybillsComponent},
  { path: '404', component: FororforComponent},
  {path: '**', redirectTo: '/404'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
